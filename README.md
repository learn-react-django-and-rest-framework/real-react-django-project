Welcome to REAL REACT DJANGO PROJECT!

This is it, for those of you who have been following the tutorials I have in my previous projects.
If you don't know what I'm talking about, scroll down to the bottom, where you'll find Back-End 101, and Front-End 101. Those are gonna be helpful before you start this!


Now, in previous projects, I provided the base for this final event. Here we are, you made it, let's get to coding. Inside this project is everything you need to get started on a REAL ReactJS and Django REST project. You may be asking, what am I going to do with this? Well, who knows. Maybe you'll fall in love with coding. <3
In this project, you will be creating an application that can track GUESTS for an EVENT. You do not need to create the event unless you're up to that task! Instructions will be added at the end for those brave enough to venture onward.


THESE INSTRUCTIONS ARE ONLY FOR WINDOWS OS USERS! (I'm poor and don't have a Mac)



**TO SET UP**

Please ensure you have "Git" installed in your computer, from this link: https://gitforwindows.org/

In the GitLab project, search for an option in the Top Right called *Forks*. Click this.
Title the project whatever you'd like, then set your *Project URLs* namespace to **your username** from the dropdown. (This ensures it will be public!)
Click Fork Project at the bottom when you're ready.
Then, find the blue *Clone* button, and select it. Copy the link titled *Clone with HTTPS*, and keep it for the next step.

Open a *Terminal*. If you search your apps, you'll find one called "terminal"... There is a terminal that comes with your VSCode, but we don't suggest using that to start.

*Windows Terminal:*

    1. Open the terminal app. It looks intimidating, but it won't be after long.
    2. THIS STEP VARIES! TRY a. FIRST, THEN TRY b. IF YOU RECEIVE AN ERROR!
        a. type "cd desktop"
        b. type "cd onedrive" THEN "cd desktop"

Once you're in the right spot, type "git clone", then paste your URL you copied from the GitLab project, and click enter.
Congrats! You now have a version of this project that ONLY YOU can see on your device!

In your *Terminal*, type the following:

    cd <put the name of your project here WITHOUT spaces (try - or _)>

These next steps may be a little tricky, so please pay attention closely. In your *Terminal*, type each line one at a time:

    python -m venv .venv
    ./.venv/Scripts/Activate.ps1
    pip install -r requirements

PIP will likely need to update, but feel free to ignore it - the rest of us do, too.

What did we just do here?
In the first line, you created a "Virtual Environment." This is basically a private version of your computer where you can install things without it installing everywhere. This is especially helpful for coding projects, because you don't have to worry about an installation updating a screwing up your code.
In the second line, you activated the virtual environment, affectionately called ".venv". You should have seen a green (.venv) appear at the start of your terminal's line.
Lastly, the final line installs all the required items for the project. This includes Django, Flake8, React Router DOM, and a few other useful things.

Last things last, you can just type "code ." to open the folder you're in for your code.



**TO USE**

If you've done the other tutorials first, you should know what to do here. If you haven't, but you don't know what's happening, er... Go check out those tutorials, please!

What is already done?
ReactJS is stored in the GHI folder.
A Django Project is created.
A Django App is created, too.
The Settings.py of your Django Project are all adjusted to allow for ReactJS.
And now, thanks to you installing the requirements, the necessary additions are there too.

Well, the real question is, what do I do to make it work?
You will need to use Windows Terminal, as it allows you to open multiple tabs of powershells.

*To Start the Django Project:*

In the Powershell tab you used to open your project (your last line should end in "code ."), type the following:

    python manage.py makemigrations
    python manage.py migrate
    python manage.py createsuperuser
        <enter your selected username>
        <skip the email, this is a public project>
        <enter a simple password (it will not show you typing)>
        <re-enter the password>
        <if your password is too common, type "y">
    python manage.py runserver

Now you can go to "http://localhost:8000/admin/", log in using the credentials you made, and your project will be up and running... It just won't have anything in it until you work on Models.py and Admin.py!

*To Start the React Project:*

Be sure to open a new tab, and guide it to the same directory using the following instructions:

    cd desktop (or cd onedrive->cd desktop, if you had to do that previously)
    cd real-react-django-project (if you type "real" and click tab, it should auto-fill the name of the project)
    cd ghi
    npm start

Now you can go to "http://localhost:3000/" and you'll have a project!

*Notes:*

To run the React server, you will need the Django server to be up too. Both of these must be running simultaneously to keep your project up.
If you have to migrate, you will need to do so in yet another new tab. Follow these steps to get to your project if you have to migrate:

    cd desktop (or cd onedrive->cd desktop, if you had to do that previously)
    cd real-react-django-project (if you type "real" and click tab, it should auto-fill the name of the project)

**YOU WILL NEED TO MIGRATE EVERY TIME YOU EDIT YOUR MODELS.PY**

**I REPEAT: IF YOU EDIT YOUR MODELS.PY, PLEASE RUN THE FOLLOWING:**

    python manage.py makemigrations
    python manage.py migrate


If you made something you really like and want to save it for later online...
Type the following lines, one by one, into your terminal:

    git add .
    git commit -m "Type here what you did with the project!"
    git push origin main

When it's finished loading, check out GitLab and see if it worked. Good job!


**WHEN YOU ARE DONE**

Congrats, you may (or may not) have created a Django and React project! I'm so proud of you! Now you can show everyone around you that you made something awesome.

Best of luck, baby coder!



LINK TO: Back-End-101
https://gitlab.com/learn-react-django-and-rest-framework/back-end-101

LINK TO: Front-End-101
https://gitlab.com/learn-react-django-and-rest-framework/front-end-101